﻿=== Tickera Slack Notifications ===
Contributors: tickera
Requires at least: 4.1
Tested up to: 4.4.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Receive notifications to your Slack channel whenever a sale occurs in your Tickera store.

== Description ==

Receive notifications to your Slack channel whenever a sale occurs in your Tickera store.

== Changelog ==

= 1.1 - 29/AUG/2016 =
- Fixed issues with notifications when an order is not completed right upon placing an order (with offline payments gateway, when it's marked paid from admin area etc) 

= 1.0.2 - 04/APR/2016 =
- Added plugin updater support for new licensing server

= 1.0.1 - 25/NOV/2015 =
- Added translation

= 1.0 - 24/NOV/2015 =
- First Release
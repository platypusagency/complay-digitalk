=== Tickera Mailchimp Newsletter Add-on ===
Contributors: tickera
Requires at least: 4.1
Tested up to: 4.4.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Tickera MailChimp add-on allows you to send marketing emails, automated messages, and targeted campaigns to your customers.

== Description ==

Tickera MailChimp add-on allows you to send marketing emails, automated messages, and targeted campaigns to your customers.
With the Tickera MailChimp Add-on, you can add your customers to your MailChimp e-mail lists, automatically  during the purchase process.

== Changelog ==

= 1.1.0 - 09/JAN/2017 =
- Fixed issue with sending confirmation mail when box is not checked on WooBridge

= 1.0.9 - 30/SEP/2016 =
- Removed error_log from mailchimp class

= 1.0.8 - 19/AUG/2016 =
- Fixed issue with checkbox showing even if the plugin is disabled 

= 1.0.7 - 5/AUG/2016 =
- Fixed issue with conflicts with other plugins 

= 1.0.6 - 2/AUG/2016 =
- Code update

= 1.0.5 - 1/AUG/2016 =
-Fixed issue with plugin subscribing even when there is no ticket in the cart (WooCommerce)


= 1.0.4 - 27/MAY/2016 =
-Fixed issue with plugin not saving settings

= 1.0.3 - 16/MAY/2016 =
-Fixed issue with add-on translation

= 1.0.2 - 18/APR/2016 =
- Added support for WooCommerce
- Fixed issue with missing file

= 1.0.1 - 04/APR/2016 =
- Added plugin updater support for new licensing server

= 1.0.0.2 - 23/NOV/2015 =
- Added subscription confirmation checkbox

= 1.0.0.1 - 23/NOV/2015 =
- Added automatic updater


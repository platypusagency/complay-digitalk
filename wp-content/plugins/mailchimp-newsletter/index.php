<?php
/*
  Plugin Name: Tickera Mailchimp
  Plugin URI: http://tickera.com/
  Description: Tickera MailChimp Newsletter add-on allows you to send marketing emails, automated messages, and targeted campaigns to your customers.
  Author: Tickera.com
  Author URI: http://tickera.com/
  Version: 1.1.0

  Copyright 2016 Tickera (http://tickera.com/)
 */

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

if (!class_exists('TC_Mailchimp')) {

    class TC_Mailchimp {

        var $plugin_name = 'mailchimp';
        var $admin_name = 'Mailchimp';
        var $version = '1.0.9';
        var $title = 'Tickera Mailchimp';
        var $name = 'tc';
        var $dir_name = 'tickera-mailchimp';
        var $location = 'plugins';
        var $plugin_dir = '';
        var $plugin_url = '';

        function init() {
            global $tc;


            //localize the plugin
            add_action('init', array(&$this, 'localization'), 10);

            $this->admin_name = __('Mailchimp', 'tc-mailchimp');
            $this->public_name = __('Mailchimp', 'tc-mailchimp');
        }

        //Plugin localization function
        function localization() {

// Load up the localization file if we're using WordPress in a different language
// Place it in this plugin's "languages" folder and name it "tc-[value in wp-config].mo"
            if ($this->location == 'mu-plugins') {
                load_muplugin_textdomain('tc-mailchimp', 'languages/');
            } else if ($this->location == 'subfolder-plugins') {
                //load_plugin_textdomain( 'tc-mailchimp', false, $this->plugin_dir . '/languages/' );
                load_plugin_textdomain('tc-mailchimp', false, dirname(plugin_basename(__FILE__)) . '/languages/');
            } else if ($this->location == 'plugins') {
                load_plugin_textdomain('tc-mailchimp', false, 'languages/');
            } else {
                
            }

            $temp_locales = explode('_', get_locale());
            $this->language = ($temp_locales[0]) ? $temp_locales[0] : 'en';
        }

        function __construct() {
            $this->init();
            add_filter('tc_settings_new_menus', array(&$this, 'tc_settings_new_menus_additional'));
            add_action('tc_settings_menu_tickera_mailchimp', array(&$this, 'tc_settings_menu_tickera_mailchimp_show_page'));
            add_action('tc_order_created', array(&$this, 'tc_order_created'), 10, 5);
            add_action('tc_before_cart_submit', array(&$this, 'tc_add_mailchimp_field'));
            add_action('tc_cart_passed_successfully', array(&$this, 'tc_check_confirmation'), 0);
            add_action('woocommerce_checkout_process', array(&$this, 'tc_check_confirmation'), 10, 1);
            add_action('woocommerce_new_order', array(&$this, 'tc_subscribe_to_mailchimp'), 20, 1);
            add_action('woocommerce_resume_order', array(&$this, 'tc_subscribe_to_mailchimp'), 20, 1);
            add_action('woocommerce_api_create_order', array(&$this, 'tc_subscribe_to_mailchimp'), 20, 1);
        }

        function tc_check_confirmation() {
            global $tc;

            $tc->start_session();

            $tc_mailchimp_settings = get_option('tc_mailchimp_settings');
            
            if(isset($tc_mailchimp_settings['enable_confirmation'])){
                $tc_mailchimp_confirmation = $tc_mailchimp_settings['enable_confirmation'];
            } else {
                $tc_mailchimp_confirmation = '';
            }
            //Check if confirmation is needed get_option etc.


            if ($tc_mailchimp_confirmation) {//NEED CONFIRMATION
                if (isset($_POST['tc-mailchimp-subscribe'])) {
                    $_SESSION['tc_mailchimp_confirmed_subscription'] = true;
                } else {
                    $_SESSION['tc_mailchimp_confirmed_subscription'] = false;
                }
            } else {
                
            }
        }

//tc_check_confirmation()

        function tc_settings_new_menus_additional($settings_tabs) {
            $settings_tabs['tickera_mailchimp'] = __('Mailchimp', 'tc');
            return $settings_tabs;
        }

        function tc_add_mailchimp_field() {
            $tc_mailchimp_settings = get_option('tc_mailchimp_settings');
            
            if(isset($tc_mailchimp_settings['enable_confirmation'])){
                $tc_mailchimp_confirmation = $tc_mailchimp_settings['enable_confirmation'];
            } else {
                $tc_mailchimp_confirmation = '';
            }

            if ($tc_mailchimp_confirmation && !isset($tc_mailchimp_settings['disable_mailchimp'])) {
                ?>
                <label>
                    <input type="checkbox" name="tc-mailchimp-subscribe" value="1" checked /> <?php _e('Sign-up to our newsletter.', 'tc-mailchimp'); ?>
                </label>
                <?php
            } //if ( $tc_mailchimp_confirmation )
        }

//function tc_add_mailchimp_field()
        //set mailchimp options
        function tc_settings_menu_tickera_mailchimp_show_page() {
            require_once( $this->plugin_dir . 'includes/admin-pages/settings-tickera_mailchimp.php' );
        }

//function tc_settings_menu_tickera_mailchimp_show_page()
        //send data to mailchimp
        function tc_order_created($order_id, $status, $cart_contents, $cart_info, $payment_info) {

            $tc_mailchimp_settings = get_option('tc_mailchimp_settings');

            if (isset($_SESSION['tc_mailchimp_confirmed_subscription']) && $_SESSION['tc_mailchimp_confirmed_subscription'] == false && $tc_mailchimp_settings['enable_confirmation'] == true) {
                return;
            }

            if (!empty($tc_mailchimp_settings['api_key']) && !empty($tc_mailchimp_settings['list_id']) && !isset($tc_mailchimp_settings['disable_mailchimp'])) {

                $buyer_first_name = $cart_info['buyer_data']['first_name_post_meta'];
                $buyer_last_name = $cart_info['buyer_data']['last_name_post_meta'];
                $buyer_email = $cart_info['buyer_data']['email_post_meta'];

                $api_key = $tc_mailchimp_settings['api_key'];
                $list_id = $tc_mailchimp_settings['list_id'];

                if (isset($tc_mailchimp_settings['double_optin'])) {
                    $double_optin = TRUE;
                } else {
                    $double_optin = FALSE;
                }

                //check to see if mailchimp should send welcome e-mail
                if (isset($tc_mailchimp_settings['send_welcome'])) {
                    $tc_send_welcome = TRUE;
                } else {
                    $tc_send_welcome = FALSE;
                }

                
                if (!class_exists('Mailchimp')) {
                    require_once($this->plugin_dir . 'includes/scripts/mailchimp-api/src/Mailchimp.php');
                }
                try {

                    $Mailchimp = new Mailchimp($api_key);
                    $merge_vars = array("FNAME" => $buyer_first_name, "LNAME" => $buyer_last_name);
                    $subscriber = $Mailchimp->lists->subscribe($list_id, array('email' => $buyer_email), $merge_vars, 'html', $double_optin, true, true, $tc_send_welcome);

                    if (!empty($subscriber['leid'])) {
                        // Success
                    }
                } catch (Exception $e) {
                    
                }
            }
        }

//function tc_order_created( $order_id, $status, $cart_contents, $cart_info, $payment_info )

        function tc_subscribe_to_mailchimp($order_id) {
            global $woocommerce;
            $tc_mailchimp_settings = get_option('tc_mailchimp_settings');
            if (isset($_SESSION['tc_mailchimp_confirmed_subscription']) && $_SESSION['tc_mailchimp_confirmed_subscription'] == false && $tc_mailchimp_settings['enable_confirmation'] == true) {
                return;
            }
            
            $buyer_email = $_POST['billing_email'];
            $buyer_first_name = $_POST['billing_first_name'];
            $buyer_last_name = $_POST['billing_last_name'];

            $tc_tickets_instances_arg = array(
                'post_parent' => $order_id,
                'post_type' => 'tc_tickets_instances',
                'posts_per_page' => -1,
            );
            $tc_tickets_instances = get_posts($tc_tickets_instances_arg);
            $ticket_buying_subscribe = $tc_mailchimp_settings['users_buying_tickets'];
            $tc_ticket_instance = $tc_tickets_instances[0]->post_type;

            if (!empty($tc_tickets_instances)) {
                if ($ticket_buying_subscribe == 1 && $tc_ticket_instance == 'tc_tickets_instances') {
                    $this->tc_woo_bridge_mailchimp($buyer_first_name, $buyer_last_name, $buyer_email);
                } else if (!isset($ticket_buying_subscribe)) {
                    $this->tc_woo_bridge_mailchimp($buyer_first_name, $buyer_last_name, $buyer_email);
                }
            }
        }

//function tc_subscribe_to_mailchimp($order_id)

        function tc_woo_bridge_mailchimp($buyer_first_name, $buyer_last_name, $buyer_email) {

            $tc_mailchimp_settings = get_option('tc_mailchimp_settings');
                
            if (!empty($tc_mailchimp_settings['api_key']) && !empty($tc_mailchimp_settings['list_id']) && !isset($tc_mailchimp_settings['disable_mailchimp'])) {

                $api_key = $tc_mailchimp_settings['api_key'];
                $list_id = $tc_mailchimp_settings['list_id'];

                if (isset($tc_mailchimp_settings['double_optin'])) {
                    $double_optin = TRUE;
                } else {
                    $double_optin = FALSE;
                }

                //check to see if mailchimp should send welcome e-mail
                if (isset($tc_mailchimp_settings['send_welcome'])) {
                    $tc_send_welcome = TRUE;
                } else {
                    $tc_send_welcome = FALSE;
                }

                include($this->plugin_dir . 'includes/scripts/mailchimp-api/src/Mailchimp.php');

                try {

                    $Mailchimp = new Mailchimp($api_key);
                    $merge_vars = array("FNAME" => $buyer_first_name, "LNAME" => $buyer_last_name);
                    $subscriber = $Mailchimp->lists->subscribe($list_id, array('email' => $buyer_email), $merge_vars, 'html', $double_optin, true, true, $tc_send_welcome);

                    if (!empty($subscriber['leid'])) {
                        // Success
                    }
                } catch (Exception $e) {
                    
                }
            }
        }

    }

}


$tc_mailchimp = new TC_Mailchimp();


//CHECKING MAILCHIMP API
//ajax part
add_action('admin_enqueue_scripts', 'tc_check_mailchimp');

function tc_check_mailchimp() {
    wp_enqueue_script('mailchimp-js', plugin_dir_url(__FILE__) . '/includes/scripts/javascript.js');
}

//tc_check_mailchimp 
//php part called by the ajax for checking mailchimp
add_action('wp_ajax_ajax_mailchimp_check', 'check_mailchimp_check');

function check_mailchimp_check() {

    $tc_api_key = $_POST['tc_api_key'];
    $tc_list_id = $_POST['tc_list_id'];

    include(plugin_dir_path(__FILE__) . 'includes/scripts/mailchimp-api/src/Mailchimp.php');

    try {

        $Mailchimp = new Mailchimp($tc_api_key);
        $subscriber = $Mailchimp->lists->subscribe($tc_list_id, array('email' => get_option('admin_email')));

        echo "Works Fine!";
    } catch (Exception $e) {
        echo '<span style="color:red;">' . $e->getMessage() . '</span>';
    }

    wp_die(); // this is required to terminate immediately and return a proper response
}

//Addon updater 
if (function_exists('tc_plugin_updater')) {
    tc_plugin_updater('mailchimp-newsletter', __FILE__);
}
?>